package com.bloxgaming.spigot.SuperSimpleHomes.permissions

import com.bloxgaming.spigot.SuperSimpleHomes.SuperSimpleHomes
import org.bukkit.entity.Player

/**
 * PermissionUtils
 * @author Gregory Maddra
 * 2017-01-28
 */
object PermissionUtils {

    fun hasHomePermission(player: Player, id: Int, checks: Int = SuperSimpleHomes.permCheckIterations): Boolean {
        (id..(id + checks))
                .filter { player.hasPermission("homes.teleport.$it") }
                .forEach { return true }
        return false
    }
}
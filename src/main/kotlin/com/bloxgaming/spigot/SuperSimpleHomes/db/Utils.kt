package com.bloxgaming.spigot.SuperSimpleHomes.db

import com.bloxgaming.spigot.SuperSimpleHomes.Home
import com.bloxgaming.spigot.SuperSimpleHomes.Statements
import com.bloxgaming.spigot.SuperSimpleHomes.SuperSimpleHomes
import com.bloxgaming.spigot.SuperSimpleHomes.permissions.PermissionUtils
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import java.sql.Connection
import java.util.*

/**
 * Utils
 * @author Gregory Maddra
 * 2017-01-21
 */
object Utils {

    fun getHome(player: Player, id: Int = 1): Home {
        if (PermissionUtils.hasHomePermission(player, id)) {
            var connection: Connection? = null
            try {
                connection = SuperSimpleHomes.pool.connection
                val statement = connection.prepareStatement(Statements.FIND_HOME)
                statement.setString(1, player.uniqueId.toString())
                statement.setInt(2, id)
                val results = statement.executeQuery()
                while (results.next()) {
                    val x = results.getDouble("x")
                    val y = results.getDouble("y")
                    val z = results.getDouble("z")
                    val w = results.getObject("world") as UUID
                    statement?.close()
                    return Home(x, y, z, (SuperSimpleHomes.plugin as JavaPlugin).server.getWorld(w))
                }
            } finally {
                connection?.close()
            }
        } else {
            return Home(perms = false)
        }
        return Home(valid = false)
    }

    fun getHomeByName(player: Player, name: String): Home {
        var connection: Connection? = null
        try {
            connection = SuperSimpleHomes.pool.connection
            val statement = connection.prepareStatement(Statements.FIND_HOME_BY_NAME)
            statement.setString(1, player.uniqueId.toString())
            statement.setString(2, name)
            val results = statement.executeQuery()
            while (results.next()) {
                val h = getHome(player, results.getInt("homeNum"))
                statement.close()
                return h
            }
        } finally {
            connection?.close()
        }
        return Home(valid = false)
    }

    fun checkHomeExists(player: Player, id: Int, connection: Connection): Boolean{
        try {
            val statement = connection.prepareStatement(Statements.FIND_HOME)
            statement.setString(1, player.uniqueId.toString())
            statement.setInt(2, id)
            val resultSet = statement.executeQuery()
            if(resultSet.next()){
                statement.close()
                return true
            }
        } finally {

        }
        return false
    }

    fun checkHomeExistsByName(player: Player, id: String): Boolean {
        var connection: Connection? = null
        try {
            connection = SuperSimpleHomes.pool.connection
            val statement = connection.prepareStatement(Statements.FIND_HOME_BY_NAME)
            statement.setString(1, player.uniqueId.toString())
            statement.setString(2, id)
            val results = statement.executeQuery()
            if (results.next()) {
                statement.close()
                return true
            }
        } finally {
            connection?.close()
        }
        return false
    }

    fun runPreparedSQLStatement(preparedStatement: String) {
        var connection: Connection? = null
        try {
            connection = SuperSimpleHomes.pool.connection
            val statement = connection.prepareStatement(preparedStatement)
            statement.execute()
            statement.close()
        } finally {
            connection?.close()
        }
    }
}
package com.bloxgaming.spigot.SuperSimpleHomes.db

import com.bloxgaming.spigot.SuperSimpleHomes.SuperSimpleHomes
import com.zaxxer.hikari.HikariDataSource
import java.io.File

/**
 * ConnectionPool
 * @author Gregory Maddra
 * 2017-01-21
 */
object ConnectionPool {

    var pool = HikariDataSource()
    private var inited = false

    fun init(){
        if(inited){
            return
        }
        Class.forName("org.h2.Driver")
        pool.dataSourceClassName = "org.h2.jdbcx.JdbcDataSource"
        pool.addDataSourceProperty("URL", "jdbc:h2:file:${File(SuperSimpleHomes.fileDir, "database").absolutePath};MV_STORE=FALSE")
        pool.username = "sa"
        pool.password = ""
        pool.maximumPoolSize = 5
        inited = true
    }
}
package com.bloxgaming.spigot.SuperSimpleHomes

import org.bukkit.Location
import org.bukkit.World
import org.bukkit.plugin.java.JavaPlugin

/**
 * Home
 * @author Gregory Maddra
 * 2017-01-21
 */
class Home(xPos: Double = 0.0, yPos: Double = 0.0, zPos: Double = 0.0, world: World = defaultWorld, val valid: Boolean = true, val perms: Boolean = true) {

    companion object {
        val defaultWorld = (SuperSimpleHomes.plugin as JavaPlugin).server.worlds[0]
    }

    val x: Double = xPos
    val y: Double = yPos
    val z: Double = zPos
    val w: World = world

    val location: Location by lazy {
        Location(w, x, y, z)
    }


}
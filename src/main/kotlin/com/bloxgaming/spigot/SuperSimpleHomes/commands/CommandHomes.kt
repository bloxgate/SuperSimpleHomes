package com.bloxgaming.spigot.SuperSimpleHomes.commands

import com.bloxgaming.spigot.SuperSimpleHomes.Statements
import com.bloxgaming.spigot.SuperSimpleHomes.SuperSimpleHomes
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import java.sql.Connection
import java.util.*

/**
 * CommandHomes
 * @author Gregory Maddra
 * 2017-01-21
 */
class CommandHomes : CommandExecutor{

    override fun onCommand(sender: CommandSender?, command: Command?, label: String?, args: Array<out String>?): Boolean {
        if(sender is ConsoleCommandSender){
            sender.sendMessage("This command can only be run by a player!")
        } else if (sender is Player){
            var connection: Connection? = null
            try {
                connection = SuperSimpleHomes.pool.connection
                val statement = connection.prepareStatement(Statements.FIND_ALL_HOMES)
                statement.setString(1, sender.uniqueId.toString())
                val resultSet = statement.executeQuery()
                while(resultSet.next()){
                    val x = Math.round(resultSet.getDouble("x"))
                    val y = Math.round(resultSet.getDouble("y"))
                    val z = Math.round(resultSet.getDouble("z"))
                    val w = (SuperSimpleHomes.plugin as JavaPlugin).server.getWorld(resultSet.getObject("world") as UUID)
                    val num = resultSet.getInt("homeNum")
                    val name = resultSet.getString("name")
                    val displayName = if (name == "" || name == "null") num.toString() else name
                    sender.sendMessage("${ChatColor.YELLOW}Home $displayName: X:${ChatColor.WHITE}$x ${ChatColor.YELLOW}Y:${ChatColor.WHITE}$y " +
                            "${ChatColor.YELLOW}Z:${ChatColor.WHITE}$z ${ChatColor.YELLOW}World: ${ChatColor.WHITE}${w.name}")
                }
                statement.close()
            } finally {
                connection?.close()
            }
        }
        return true
    }
}
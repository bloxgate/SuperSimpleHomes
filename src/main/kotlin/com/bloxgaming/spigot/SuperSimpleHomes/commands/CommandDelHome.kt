package com.bloxgaming.spigot.SuperSimpleHomes.commands

import com.bloxgaming.spigot.SuperSimpleHomes.Statements
import com.bloxgaming.spigot.SuperSimpleHomes.SuperSimpleHomes
import com.bloxgaming.spigot.SuperSimpleHomes.db.Utils
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player

/**
 * CommandDelHome
 * @author Gregory Maddra
 * 2017-01-21
 */
class CommandDelHome : CommandExecutor{

    override fun onCommand(sender: CommandSender?, command: Command?, label: String?, args: Array<out String>?): Boolean {
        if(args!!.isEmpty()){
            return false
        }
        if(sender is ConsoleCommandSender){
            sender.sendMessage("Only players can use this command!")
        }
        if(sender is Player){
            val connection = SuperSimpleHomes.pool.connection
            try {
                if (Utils.checkHomeExists(sender, args[0].toInt(), connection)) {
                    val statement = connection.prepareStatement(Statements.DELETE_HOME)
                    statement.setString(1, sender.uniqueId.toString())
                    statement.setInt(2, args[0].toInt())
                    statement.executeUpdate()
                    statement.close()
                    sender.sendMessage("${ChatColor.GREEN}Home ${args[0]} has been deleted")
                } else {
                    sender.sendMessage("${ChatColor.RED}Home ${args[0]} does not exist.")
                }
            } catch(e: NumberFormatException) {
                if (Utils.checkHomeExistsByName(sender, args[0])) {
                    val statement = connection.prepareStatement(Statements.DELETE_HOME_BY_NAME)
                    statement.setString(1, sender.uniqueId.toString())
                    statement.setString(2, args[0])
                    statement.executeUpdate()
                    statement.close()
                    sender.sendMessage("${ChatColor.GREEN}Home ${args[0]} has been deleted")
                } else {
                    sender.sendMessage("${ChatColor.RED}Home ${args[0]} does not exist")
                }
            } finally {
                connection?.close()
            }
        }
        return true
    }
}
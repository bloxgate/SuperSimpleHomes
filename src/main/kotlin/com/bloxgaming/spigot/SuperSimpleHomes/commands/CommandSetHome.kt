package com.bloxgaming.spigot.SuperSimpleHomes.commands

import com.bloxgaming.spigot.SuperSimpleHomes.Statements
import com.bloxgaming.spigot.SuperSimpleHomes.SuperSimpleHomes
import com.bloxgaming.spigot.SuperSimpleHomes.db.Utils
import com.bloxgaming.spigot.SuperSimpleHomes.permissions.PermissionUtils
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import java.sql.Connection
import java.sql.PreparedStatement

/**
 * CommandSetHome
 * @author Gregory Maddra
 * 2017-01-21
 */
class CommandSetHome : CommandExecutor{
    override fun onCommand(sender: CommandSender?, command: Command?, label: String?, args: Array<out String>?): Boolean {
        if(args!!.isEmpty()){
            return false
        }
        val id by lazy {
            try {
                args[0].toInt()
            } catch (ex: NumberFormatException) {
                args[0]
            }
        }
        val name: String
        if (args.size == 2) {
            name = args[1]
        } else {
            name = ""
        }
        if(sender is ConsoleCommandSender){
            sender.sendMessage("Only players can use that command!")
        } else if(sender is Player){
            if (id is Int)
                integerID(id, sender, name)
            else
                stringID(id as String, sender)
        }
        return true
    }

    fun integerID(id: Int, sender: Player, Name: String?) {
        var name = Name
        if (!PermissionUtils.hasHomePermission(sender, id)) {
            sender.sendMessage("${ChatColor.RED}You do not have permission to have that many homes")
            return
        }
        var connection: Connection? = null
        try {
            connection = SuperSimpleHomes.pool.connection
            if (Utils.checkHomeExists(sender, id, connection)) {
                var statement: PreparedStatement?

                //Get existing name
                if (name == "" || name == null) {
                    statement = connection.prepareStatement(Statements.GET_HOME_NAME)
                    statement.setString(1, sender.uniqueId.toString())
                    statement.setInt(2, id)
                    val result = statement.executeQuery()
                    while (result.next()) {
                        name = result.getString("name")
                    }
                }

                //Update home
                statement = connection.prepareStatement(Statements.UPDATE_HOME)
                statement!!.setDouble(1, sender.location.x)
                statement.setDouble(2, sender.location.y)
                statement.setDouble(3, sender.location.z)
                statement.setObject(4, sender.world.uid)
                statement.setString(5, name)
                statement.setString(6, sender.uniqueId.toString())
                statement.setInt(7, id)
                statement.executeUpdate()
                statement.close()

                sender.sendMessage("${ChatColor.GREEN}Home updated successfully!")
            } else {
                //check for home with same name
                if (name != "") {
                    val statement = connection.prepareStatement(Statements.FIND_HOME_BY_NAME)
                    statement.setString(1, sender.uniqueId.toString())
                    statement.setString(2, name)
                    val result = statement.executeQuery()
                    while (result.next()) {
                        val dbNum = result.getInt("homeNum")
                        if (dbNum != id) {
                            sender.sendMessage("${ChatColor.RED}Home name already in use!")
                            return
                        }
                    }
                }

                val statement = connection.prepareStatement(Statements.ADD_HOME)
                statement!!.setString(1, sender.uniqueId.toString())
                statement.setInt(2, id)
                statement.setString(3, name)
                statement.setDouble(4, sender.location.x)
                statement.setDouble(5, sender.location.y)
                statement.setDouble(6, sender.location.z)
                statement.setObject(7, sender.world.uid)
                statement.executeUpdate()
                statement.close()
            }
            sender.sendMessage("${ChatColor.GREEN}Home set successfully!")
        } finally {
            connection?.close()
        }
    }

    fun stringID(id: String, sender: Player) {
        var connection: Connection? = null
        try {
            connection = SuperSimpleHomes.pool.connection
            val statement = connection.prepareStatement(Statements.FIND_HOME_BY_NAME)
            statement.setString(1, sender.uniqueId.toString())
            statement.setString(2, id)
            val results = statement.executeQuery()
            if (results.next()) {
                integerID(results.getInt("homeNum"), sender, id)
            } else {
                sender.sendMessage("${ChatColor.RED}No home exists with that name")
            }
        } finally {
            connection?.close()
        }
    }
}